It is a movies demo application which uses The Movie Database API (https://developers.themoviedb.org/3/getting-started/introduction) as a data source.
The app starts by displaying a list of movies currently in theaters. It is a silently paginated list. 

![list view](pics/list.png)

A specific film can be found by using a search view.

![search autocompletion](pics/autocompletion.png)
![search](pics/search.png)

To view details about a film just tap on a poster.

![details view](pics/details.png)

You can get the release apk in the directory /app/release/app-release.apk. Min SDK version 19.

For implementation of this app, I used MVVM + Interactor architecture and data binding. For processing network requests I used retrofit, okhttp and rxjava.