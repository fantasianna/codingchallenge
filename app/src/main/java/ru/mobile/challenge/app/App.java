package ru.mobile.challenge.app;

import android.app.Application;
import android.content.Context;

import ru.mobile.challenge.di.DaggerCore;

public class App extends Application {

    private static Context sAppContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sAppContext = this;
        DaggerCore.init();
    }

    public static Context getContext() {
        return sAppContext;
    }
}
