package ru.mobile.challenge.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import ru.mobile.challenge.R;
import ru.mobile.challenge.app.App;
import ru.mobile.challenge.network.DataConverter;

public class MovieModel implements Parcelable {
    private String title;
    private String overview;
    private String releaseDate;

    private String posterUrl;
    private String backdropUrl;

    public MovieModel() {
    }

    protected MovieModel(Parcel in) {
        title = in.readString();
        overview = in.readString();
        releaseDate = in.readString();
        posterUrl = in.readString();
        backdropUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(overview);
        dest.writeString(releaseDate);
        dest.writeString(posterUrl);
        dest.writeString(backdropUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MovieModel> CREATOR = new Creator<MovieModel>() {
        @Override
        public MovieModel createFromParcel(Parcel in) {
            return new MovieModel(in);
        }

        @Override
        public MovieModel[] newArray(int size) {
            return new MovieModel[size];
        }
    };

    public String getTitle() {
        if (releaseDate == null) {
            return title;
        }
        else {
            final Calendar calendar = new GregorianCalendar();
            calendar.setTime(getReleaseDate());
            final int year = calendar.get(Calendar.YEAR);
            return String.format("%s (%s)", title, year);
        }
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(final String overview) {
        this.overview = overview;
    }

    public Date getReleaseDate() {
        return DataConverter.convertServerDate(releaseDate);
    }

    public void setReleaseDate(final String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setImages(final Context context, final String posterPath, final String backdropPath) {
        final String apiKey = context.getString(R.string.movies_db_api_key);
        this.posterUrl = context.getString(
                            R.string.small_image_uri_mask, posterPath, apiKey);
        this.backdropUrl = context.getString(
                            R.string.big_image_uri_mask, backdropPath, apiKey);
    }

    public String getBackdropUrl() {
        return backdropUrl;
    }

    public String getPosterUrl() {
        return posterUrl;
    }
}
