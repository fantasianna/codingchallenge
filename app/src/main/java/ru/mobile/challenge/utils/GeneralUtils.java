package ru.mobile.challenge.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresPermission;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import ru.mobile.challenge.app.App;


public final class GeneralUtils {

    private GeneralUtils() {
        // No instances
    }

    public static void hideSoftKeyboard(@NonNull final Activity activity) {
        hideSoftKeyboard(activity, false, activity.getWindow()
                .getDecorView());
    }

    public static void hideSoftKeyboard(@NonNull final Context context, @NonNull final View... views) {
        hideSoftKeyboard(context, true, views);
    }

    public static void hideSoftKeyboard(@NonNull final Context context, final boolean clearFocus,
                                        @NonNull final View... views) {
        final InputMethodManager manager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        for (final View currentView : views) {
            if (null == currentView) {
                continue;
            }
            if (clearFocus) {
                currentView.clearFocus();
            }
            manager.hideSoftInputFromWindow(currentView.getWindowToken(), 0);
            manager.hideSoftInputFromWindow(currentView.getApplicationWindowToken(), 0);
        }
    }

    public static void openSettings() {
        App.getContext()
                .startActivity(new Intent(Settings.ACTION_SETTINGS).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public static void openAppSettings() {
        final Context context = App.getContext();
        final Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setData(Uri.parse("package:" + context.getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(intent);
    }

    public static void showSoftKeyboard(@NonNull final View view) {
        final InputMethodManager manager = (InputMethodManager) App.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        view.requestFocus();
        manager.showSoftInput(view, 0);
    }

    @RequiresPermission(Manifest.permission.ACCESS_NETWORK_STATE)
    public static boolean isOnline() {
        final ConnectivityManager connectivityManager = (ConnectivityManager) App.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }
}
