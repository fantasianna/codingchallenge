package ru.mobile.challenge.databinding;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.mobile.challenge.ui.base.listeners.ItemClickSupport;


/**
 * Binder for databinding
 */
public final class DataBinder {

    private DataBinder() {
        // No instances
    }

    @BindingAdapter("visibility")
    public static void setVisibility(@NonNull final View view, final boolean isVisible) {
        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    // RecyclerView
    @BindingAdapter("hasFixedSize")
    public static void setHasFixedSize(@NonNull final RecyclerView recyclerView, final boolean isFixedSize) {
        recyclerView.setHasFixedSize(isFixedSize);
    }

    @BindingAdapter("onRefreshListener")
    public static void setRefreshListener(@NonNull final SwipeRefreshLayout swipeRefreshLayout,
            @NonNull final SwipeRefreshLayout.OnRefreshListener onRefreshListener) {
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
    }

    @BindingAdapter("onScrollListener")
    public static void setScrollListener(@NonNull final RecyclerView recyclerView,
            @NonNull final RecyclerView.OnScrollListener onScrollListener) {
        recyclerView.addOnScrollListener(onScrollListener);
    }

    @BindingAdapter("onItemClickListener")
    public static void setItemClickListener(@NonNull final RecyclerView recyclerView,
            @NonNull final ItemClickSupport.OnItemClickListener onItemClickListener) {
        ItemClickSupport.addTo(recyclerView)
                .setOnItemClickListener(onItemClickListener);
    }
}
