package ru.mobile.challenge.rx;

import android.support.annotation.Nullable;

import ru.mobile.challenge.ui.base.viewmodel.IBaseViewModel;

import java.lang.ref.WeakReference;

import rx.Observer;

public abstract class BaseObserver<Subscription> implements Observer<Subscription> {

    /**
     * Reference to {@link IBaseViewModel}. Prevent possible memory leaks
     */
    protected final WeakReference<IBaseViewModel> mWeakViewModel;

    public BaseObserver(@Nullable final IBaseViewModel viewModel, boolean silently) {
        mWeakViewModel = new WeakReference<>(viewModel);
        if (viewModel != null && !silently) {
            viewModel.onLoadingStarted();
        }
    }

    @Override
    public void onCompleted() {
        final IBaseViewModel viewModel = mWeakViewModel.get();
        if (viewModel != null) viewModel.onLoadingCompleted();
    }

    @Override
    public void onError(final Throwable e) {
        // pass
    }

    @Override
    public void onNext(final Subscription instance) {
        // Override if need
    }

}
