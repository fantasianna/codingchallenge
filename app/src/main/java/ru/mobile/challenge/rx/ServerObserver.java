package ru.mobile.challenge.rx;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.net.SocketTimeoutException;

import ru.mobile.challenge.R;
import ru.mobile.challenge.app.App;
import ru.mobile.challenge.network.exceptions.NoNetworkException;
import ru.mobile.challenge.network.exceptions.RequestTimeoutException;
import ru.mobile.challenge.network.exceptions.UnknownException;
import ru.mobile.challenge.ui.base.viewmodel.IBaseViewModel;
import retrofit2.Response;

/**
 * Observer for server requests
 *
 * @param <ResponseData> server model is located in retrofit2.Response#body()
 */
public class ServerObserver<ResponseData> extends BaseObserver<Response<ResponseData>> {

    public ServerObserver(@Nullable final IBaseViewModel viewModel, boolean silently) {
        super(viewModel, silently);
    }

    @Override
    @CallSuper
    public void onNext(final Response<ResponseData> response) {
        super.onNext(response);
        if (response.isSuccessful()) {
            onSuccess(response.body());
        } else {
            onFailure(response.toString());
        }
    }

    private void onSuccess(@NonNull final ResponseData responseBody) {
        if (responseBody == null) {
            onFailure(App.getContext()
                    .getString(R.string.error_something_wrong));
        } else {
            onResponse(responseBody);
        }
    }

    /**
     * Called if response not null.
     */
    protected void onResponse(@NonNull final ResponseData responseData) {
        // Should be overridden in case.
    }

    protected void onFailure(@NonNull final String errorStatus) {
        final IBaseViewModel viewModel = mWeakViewModel.get();
        if (viewModel != null) {
            handleError(viewModel, errorStatus);
        }
    }

    protected void handleError(final IBaseViewModel viewModel, @NonNull final String errorStatus) {
        viewModel.onLoadingError(errorStatus);
    }

    @Override
    public void onError(final Throwable e) {
        super.onError(e);

        final IBaseViewModel viewModel = mWeakViewModel.get();
        if (viewModel != null) {
            if (isTimeout(e)) {
                viewModel.onLoadingError(new RequestTimeoutException(e.getMessage()));
            } else if (isNetworkProblem(e)) {
                viewModel.onLoadingError(new NoNetworkException(e));
            }
            else {
                viewModel.onLoadingError(new UnknownException(e));
            }
        }
    }

    /**
     * Check is it network problem
     *
     * @param throwable exception from onFailure
     * @return is it network problem
     */
    private boolean isNetworkProblem(@NonNull final Throwable throwable) {
        return throwable instanceof IOException;
    }

    /**
     * Check if transferRequest timeout
     *
     * @param throwable exception from onFailure
     * @return is transferRequest timeout
     */
    private boolean isTimeout(@NonNull final Throwable throwable) {
        return throwable instanceof SocketTimeoutException;
    }

    /**
     * Check if parse response error.
     *
     * @param throwable exception from onFailure
     * @return is parse response error
     */
    private boolean isParseResponseError(@NonNull final Throwable throwable) {
        return throwable instanceof JsonSyntaxException;
    }

}
