package ru.mobile.challenge.di.modules;

import android.support.annotation.NonNull;

import ru.mobile.challenge.network.Api;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class ChallengeModule {

    @NonNull
    @Provides
    @Singleton
    Api getApiService() {
        return Api.ServerFactory.createInstance();
    }
}
