package ru.mobile.challenge.di;

import android.support.annotation.NonNull;

import ru.mobile.challenge.di.components.CoreComponent;
import ru.mobile.challenge.di.components.DaggerCoreComponent;


public final class DaggerCore {

    private static CoreComponent sCoreComponent;

    private DaggerCore() {
        //No instances
    }

    @NonNull
    private static CoreComponent createComponent() {
        return DaggerCoreComponent.builder()
                .build();
    }

    /**
     * @return inject component
     */
    @NonNull
    public static CoreComponent component() {
        return sCoreComponent;
    }

    /**
     * Initialize application core
     */
    public static void init() {
        sCoreComponent = createComponent();
    }

}
