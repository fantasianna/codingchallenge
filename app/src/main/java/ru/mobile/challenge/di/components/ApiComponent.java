package ru.mobile.challenge.di.components;

import ru.mobile.challenge.di.modules.ChallengeModule;
import ru.mobile.challenge.network.Api;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ChallengeModule.class)
public interface ApiComponent {
    Api cinemaApi();
}
