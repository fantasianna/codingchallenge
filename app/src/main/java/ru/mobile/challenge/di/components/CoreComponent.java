package ru.mobile.challenge.di.components;

import ru.mobile.challenge.di.modules.ChallengeModule;
import ru.mobile.challenge.ui.list.ListInteractor;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ChallengeModule.class)
public interface CoreComponent {
    void inject(final ListInteractor interactor);
}
