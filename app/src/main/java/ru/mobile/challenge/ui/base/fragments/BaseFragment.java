package ru.mobile.challenge.ui.base.fragments;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.azoft.injectorlib.Injector;
import ru.mobile.challenge.ui.base.activites.BaseActivity;
import ru.mobile.challenge.ui.base.views.IBaseView;


public abstract class BaseFragment<DataBinding extends ViewDataBinding> extends Fragment
        implements IBaseView {

    private final Injector mInjector = Injector.init(getClass());
    protected DataBinding binding;

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInjector.applyRestoreInstanceState(this, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater,
                             @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    /**
     * Layout id of this fragment.
     *
     * @return layout id of this fragment.
     */
    @LayoutRes
    protected abstract int getLayoutId();

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupBinding(view);
        setupUI();
        setupToolbar();
    }

    /**
     * Init binding for this fragment(view).
     *
     * @param view layout of this fragment.
     */
    @CallSuper
    protected void setupBinding(@NonNull final View view) {
        binding = DataBindingUtil.bind(view);
    }

    /**
     * Set UI for this fragment
     */
    protected void setupUI() {
        // Empty
    }

    /**
     * Set Toolbar for this activity
     */
    protected void setupToolbar() {
        // If required
    }

    /**
     * Check for correct activity instance. Can be attached only to
     * {@link BaseActivity}  instances
     *
     * @return true if activity correct instance, throw exception otherwise.
     */
    private boolean isActivityCorrectThrowOtherwise() {
        if (getActivity() instanceof BaseActivity) {
            return true;
        } else {
            throw new IllegalStateException("This fragment can't be used outside BaseActivity");
        }
    }

    protected final void initToolbar() {
        if (isActivityCorrectThrowOtherwise()) {
            setHasOptionsMenu(true);
            ((BaseActivity) getActivity()).initToolbar();
        }
    }

    protected final void displayBackButton() {
        if (isActivityCorrectThrowOtherwise()) {
            ((BaseActivity) getActivity()).showBackButton();
        }
    }

    protected final void setToolbarTitle(String title) {
        if (isActivityCorrectThrowOtherwise()) {
            ((BaseActivity) getActivity()).setToolbarTitle(title);
        }
    }

    protected final void setToolbarTitle(@StringRes int resId) {
        if (isActivityCorrectThrowOtherwise()) {
            ((BaseActivity) getActivity()).setToolbarTitle(resId);
        }
    }

    /**
     * Calls {@link BaseActivity#showLoading()}
     */
    @Override
    @CallSuper
    public void showLoading() {
        ((IBaseView) getActivity()).showLoading();
    }

    /**
     * Calls {@link BaseActivity#hideLoading()}
     */
    @Override
    @CallSuper
    public void hideLoading() {
        ((IBaseView) getActivity()).hideLoading();
    }

    /**
     * Calls {@link BaseActivity#showNetworkDialog()}
     */
    @Override
    public void showNetworkDialog() {
        ((IBaseView) getActivity()).showNetworkDialog();
    }

    /**
     * Calls {@link BaseActivity#showError(int)}
     */
    @Override
    public void showError(@StringRes final int errorId) {
        ((IBaseView) getActivity()).showError(errorId);
    }

    /**
     * Calls {@link BaseActivity#showError(String)}
     */
    @Override
    public void showError(@NonNull final String message) {
        ((IBaseView) getActivity()).showError(message);
    }

    @Nullable
    @Override
    public Bundle getExtras() {
        return getArguments();
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        mInjector.applyOnSaveInstanceState(this, outState);
    }
}
