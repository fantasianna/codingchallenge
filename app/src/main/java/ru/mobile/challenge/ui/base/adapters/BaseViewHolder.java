package ru.mobile.challenge.ui.base.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * @param <Item> item type
 */
public abstract class BaseViewHolder<Item, Binding extends ViewDataBinding>
        extends RecyclerView.ViewHolder {

    @NonNull
    protected Binding binding;

    public BaseViewHolder(@NonNull final View view) {
        super(view);
        binding = DataBindingUtil.bind(view);
    }

    public abstract void bind(@NonNull final Item item);
}
