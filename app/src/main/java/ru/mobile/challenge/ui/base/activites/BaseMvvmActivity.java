package ru.mobile.challenge.ui.base.activites;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import ru.mobile.challenge.BR;
import ru.mobile.challenge.ui.base.viewmodel.IBaseViewModel;


public abstract class BaseMvvmActivity<ViewModel extends IBaseViewModel, DataBinding extends ViewDataBinding>
        extends BaseActivity<DataBinding> {

    protected ViewModel viewModel;

    @Override
    @CallSuper
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        viewModel = createViewModel(savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    @Override
    @CallSuper
    public void onPostCreate(@Nullable final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        viewModel.onViewModelSet();
    }

    @NonNull
    protected abstract ViewModel createViewModel(@Nullable final Bundle savedInstanceState);

    @Override
    protected void setupBinding() {
        super.setupBinding();
        binding.setVariable(BR.viewModel, viewModel);
    }

    @Override
    @CallSuper
    public void onResume() {
        super.onResume();
        viewModel.onResume();
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        viewModel.onSaveInstanceState(outState);
    }

    @Override
    @CallSuper
    public void onPause() {
        viewModel.onPause();
        super.onPause();
    }

    @Override
    @CallSuper
    public void onDestroy() {
        viewModel.onDestroy();
        viewModel = null;
        super.onDestroy();
    }

}
