package ru.mobile.challenge.ui.base.interactors;


public interface IBaseInteractor {

    void clearSubscriptions();

    void onDestroy();
}
