package ru.mobile.challenge.ui.details;


import android.support.annotation.NonNull;

import ru.mobile.challenge.model.MovieModel;
import ru.mobile.challenge.ui.base.views.IBaseView;

interface IDetailsView extends IBaseView {
    void showMovie(@NonNull final MovieModel movieModel);
}
