package ru.mobile.challenge.ui.list;

import android.support.annotation.NonNull;

import java.util.List;

import ru.mobile.challenge.model.MovieModel;
import ru.mobile.challenge.ui.base.views.IBaseView;

interface IListView extends IBaseView {
    void onItemClick(@NonNull final MovieModel movie);
    void showMovies(@NonNull final List<MovieModel> movies);
    void addMovies(@NonNull final List<MovieModel> movies);
}
