package ru.mobile.challenge.ui.list;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import ru.mobile.challenge.R;
import ru.mobile.challenge.app.App;
import ru.mobile.challenge.di.DaggerCore;
import ru.mobile.challenge.model.MovieModel;
import ru.mobile.challenge.network.Api;
import ru.mobile.challenge.network.DataConverter;
import ru.mobile.challenge.network.responses.MovieResponse;
import ru.mobile.challenge.network.responses.PagedResponse;
import ru.mobile.challenge.rx.ServerObserver;
import ru.mobile.challenge.ui.base.interactors.BaseInteractor;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ListInteractor extends BaseInteractor<IListViewModel> implements IListInteractor {

    @Inject
    public Api api;

    private String apiKey;
    private int currentPage = 1;
    private boolean hasNext;
    private boolean isLoading;
    private String lastQuery;

    private List<MovieModel> cachedMovies;

    ListInteractor(@NonNull final IListViewModel viewModel) {
        super(viewModel);

        DaggerCore.component()
                .inject(this);

        apiKey = App.getContext().getString(R.string.movies_db_api_key);
    }

    @Override
    public void loadMovies() {
        isLoading = true;
        currentPage = 1;
        lastQuery = null;
        addSubscription(api.getMovies(currentPage, apiKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ServerObserver<PagedResponse<MovieResponse>>(getViewModel(), false) {
                    @Override
                    protected void onResponse(@NonNull final PagedResponse<MovieResponse> responseData) {
                        super.onResponse(responseData);
                        hasNext = responseData.getPage() < responseData.getTotalPages() - 1;
                        cachedMovies = DataConverter.convertList(responseData.getResults());

                        if (cachedMovies.isEmpty()) {
                            getViewModel().onEmptyLoaded();
                        }
                        else {
                            getViewModel().onMoviesLoaded(cachedMovies);
                        }
                        isLoading = false;
                    }
                }));
    }

    @Override
    public void search(final String query) {
        if (query.length() < 1) {
            return;
        }
        isLoading = true;
        currentPage = 1;
        lastQuery = query;
        addSubscription(api.searchMovies(currentPage, query, apiKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ServerObserver<PagedResponse<MovieResponse>>(getViewModel(), true) {
                    @Override
                    protected void onResponse(@NonNull final PagedResponse<MovieResponse> responseData) {
                        super.onResponse(responseData);
                        onMoviesSearchResponse(responseData);
                    }
                }));
    }

    @Override
    public void loadMore() {
        if (!isLoading && hasNext) {
            ++currentPage;
            if (lastQuery == null) {
                loadMoreMovies();
            }
            else {
                loadMoreSearches();
            }
        }
    }

    @Override
    public void selectMovie(final int position) {
        getViewModel().onMovieSelected(cachedMovies.get(position));
    }

    private void onMoviesSearchResponse(@NonNull final PagedResponse<MovieResponse> responseData) {
        hasNext = responseData.getPage() < responseData.getTotalPages() - 1;
        cachedMovies = DataConverter.convertList(responseData.getResults());

        if (cachedMovies.isEmpty()) {
            getViewModel().onEmptyLoaded();
        }
        else {
            getViewModel().onMoviesLoaded(cachedMovies);
        }
        isLoading = false;
    }

    private void loadMoreMovies() {
        isLoading = true;
        addSubscription(api.getMovies(currentPage, apiKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ServerObserver<PagedResponse<MovieResponse>>(getViewModel(), true) {
                    @Override
                    protected void onResponse(@NonNull final PagedResponse<MovieResponse> responseData) {
                        super.onResponse(responseData);
                        onMoreMoviesSearchResponse(responseData);
                    }
                }));
    }

    private void loadMoreSearches() {
        isLoading = true;
        addSubscription(api.searchMovies(currentPage, lastQuery, apiKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ServerObserver<PagedResponse<MovieResponse>>(getViewModel(), true) {
                    @Override
                    protected void onResponse(@NonNull final PagedResponse<MovieResponse> responseData) {
                        super.onResponse(responseData);
                        onMoreMoviesSearchResponse(responseData);
                    }
                }));
    }

    private void onMoreMoviesSearchResponse(@NonNull final PagedResponse<MovieResponse> responseData) {
        hasNext = responseData.getPage() < responseData.getTotalPages() - 1;
        final List<MovieModel> movieModels = DataConverter.convertList(responseData.getResults());
        cachedMovies.addAll(movieModels);
        getViewModel().onMoreMoviesLoaded(movieModels);
        isLoading = false;
    }
}
