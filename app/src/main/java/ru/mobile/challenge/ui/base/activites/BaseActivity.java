package ru.mobile.challenge.ui.base.activites;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.AnimRes;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.azoft.injectorlib.Injector;
import ru.mobile.challenge.R;
import ru.mobile.challenge.ui.base.views.IBaseView;
import ru.mobile.challenge.ui.common.dialogs.InformDialog;
import ru.mobile.challenge.ui.common.dialogs.LoadingDialog;
import ru.mobile.challenge.ui.common.dialogs.NetworkDialog;
import ru.mobile.challenge.utils.GeneralUtils;


public abstract class BaseActivity<DataBinding extends ViewDataBinding> extends AppCompatActivity
        implements IBaseView {

    private final Injector mInjector = Injector.init(getClass());
    protected DataBinding binding;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mInjector.applyRestoreInstanceState(this, savedInstanceState);
        setupBinding();
        setupDataFromIntent();
        setupUI();
        setupToolbar();
    }

    /**
     * Init binding for this activity. Here we should call setContentView.
     */
    @CallSuper
    protected void setupBinding() {
        binding = DataBindingUtil.setContentView(this, getLayoutId());
    }

    @LayoutRes
    protected abstract int getLayoutId();

    /**
     * Set UI for this activity
     */
    protected void setupUI() {
        // If required
    }

    protected void setupDataFromIntent() {
        // If required
    }

    /**
     * Set Toolbar for this activity
     */
    protected void setupToolbar() {
        // If required
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        mInjector.applyOnSaveInstanceState(this, outState);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        GeneralUtils.hideSoftKeyboard(this);
    }

    public void startActivityWithAnimation(@NonNull final Intent intent) {
        startActivityWithAnimation(intent, R.anim.slide_from_right, R.anim.slide_to_left);
    }

    public void startActivityWithAnimation(@NonNull final Intent intent, @AnimRes final int animationIn,
                                           @AnimRes final int animationOut) {
        startActivity(intent);
        overridePendingTransition(animationIn, animationOut);
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideLoading();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (android.R.id.home == item.getItemId()) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public final void initToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    public final void showBackButton() {
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    public final void setToolbarTitle(final String title) {
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle(title);
        }
    }

    public final void setToolbarTitle(@StringRes final int resId) {
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle(resId);
        }
    }

    /**
     * Needs to be overridden in child if it's required
     */
    @Override
    public void showLoading() {
        LoadingDialog.build(this)
                .message(R.string.progress)
                .show();
    }

    /**
     * Needs to be overridden in child if it's required
     */
    @Override
    public void hideLoading() {
        final LoadingDialog dialog = (LoadingDialog) getSupportFragmentManager()
                .findFragmentByTag(LoadingDialog.TAG);
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void showNetworkDialog() {
        NetworkDialog.show(this);
    }

    @Override
    public void showError(@StringRes final int errorId) {
        showError(getString(errorId));
    }

    @Override
    public void showError(@NonNull final String message) {
        InformDialog.show(this, message);
    }

    @Nullable
    @Override
    public Bundle getExtras() {
        return getIntent().getExtras();
    }
}