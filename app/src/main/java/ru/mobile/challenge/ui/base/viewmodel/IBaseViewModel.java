package ru.mobile.challenge.ui.base.viewmodel;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import ru.mobile.challenge.ui.base.views.IBaseView;

public interface IBaseViewModel {

    void setUIView(@NonNull final IBaseView uiView);

    void onLoadingStarted();

    void onLoadingError(@NonNull final Exception exception);

    void onLoadingError(@NonNull final String status);

    void onLoadingError(final int code, @StringRes final int errorId);

    void onLoadingCompleted();

    void onViewModelSet();

    // TODO avoid lifecycle callbacks
    void onResume();

    void onSaveInstanceState(@NonNull final Bundle outState);

    void onPause();

    void onDestroy();

}
