package ru.mobile.challenge.ui.list;

import java.util.List;

import ru.mobile.challenge.model.MovieModel;
import ru.mobile.challenge.ui.base.viewmodel.IBaseViewModel;

public interface IListViewModel extends IBaseViewModel {
    void loadMovies();
    void tryLoadMore();
    void search(String query);

    void onMoviesLoaded(final List<MovieModel> moviesModels);
    void onMoreMoviesLoaded(final List<MovieModel> moviesModels);
    void onMovieSelected(final MovieModel movieModel);
    void onEmptyLoaded();
}
