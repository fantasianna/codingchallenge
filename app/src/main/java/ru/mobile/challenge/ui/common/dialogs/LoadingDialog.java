package ru.mobile.challenge.ui.common.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.azoft.injectorlib.InjectSavedState;

import ru.mobile.challenge.R;
import ru.mobile.challenge.ui.base.dialogs.BaseDialog;


public class LoadingDialog extends BaseDialog {

    public static final String TAG = LoadingDialog.class.getSimpleName();

    @InjectSavedState
    private String mMessage;

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        setCancelable(false);

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        if (mMessage == null){
            mMessage = getString(R.string.progress);
        }
        progressDialog.setMessage(mMessage);

        return progressDialog;
    }

    public static Builder build(final AppCompatActivity activity) {
        return new Builder(activity);
    }

    public static class Builder {

        private final AppCompatActivity mActivity;
        private final LoadingDialog mDialog;

        Builder(final AppCompatActivity activity) {
            mActivity = activity;
            mDialog = new LoadingDialog();
        }

        public void show() {
            final FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
            mDialog.show(fragmentManager, TAG);
            //fragmentManager.executePendingTransactions();
        }

        public Builder message(final int messageId) {
            message(mActivity.getString(messageId));
            return this;
        }

        public Builder message(final String message) {
            mDialog.mMessage = message;
            return this;
        }

    }

}
