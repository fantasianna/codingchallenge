package ru.mobile.challenge.ui.list;

import ru.mobile.challenge.ui.base.interactors.IBaseInteractor;

interface IListInteractor extends IBaseInteractor {
    void loadMovies();
    void loadMore();
    void selectMovie(int position);
    void search(final String query);
}
