package ru.mobile.challenge.ui.list;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import ru.mobile.challenge.model.MovieModel;
import ru.mobile.challenge.ui.base.listeners.ItemClickSupport;
import ru.mobile.challenge.ui.base.viewmodel.BaseViewModel;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

public final class ListViewModel extends BaseViewModel<IListView, IListInteractor> implements IListViewModel {

    ListViewModel(@NonNull final ListActivity baseView, @Nullable final Bundle savedInstanceState) {
        super(baseView, savedInstanceState);
    }

    @NonNull
    @Override
    protected IListInteractor createInteractor() {
        return new ListInteractor(this);
    }

    @Override
    @CallSuper
    public void onViewModelSet() {
        super.onViewModelSet();
        loadMovies();
    }

    @Override
    public void loadMovies() {
        getInteractor().loadMovies();
    }

    @Override
    public void onMoviesLoaded(final List<MovieModel> moviesModel) {
        mContentState.set(ContentState.EXIST);
        getUIView().showMovies(moviesModel);
    }

    @Override
    public void onMoreMoviesLoaded(final List<MovieModel> moviesModels) {
        getUIView().addMovies(moviesModels);
    }

    @Override
    public void onEmptyLoaded() {
        mContentState.set(ContentState.EMPTY);
    }

    @Override
    public void tryLoadMore() {
        getInteractor().loadMore();
    }

    @Override
    public void search(final String query) {
        getInteractor().search(query);
    }

    @NonNull
    public SwipeRefreshLayout.OnRefreshListener getOnRefreshListener() {
        return () -> getInteractor().loadMovies();
    }


    @NonNull
    public ItemClickSupport.OnItemClickListener getOnItemClickListener() {
        return (recyclerView, position, view) -> getInteractor().selectMovie(position);
    }

    @Override
    public void onMovieSelected(final MovieModel movieModel) {
        getUIView().onItemClick(movieModel);
    }
}
