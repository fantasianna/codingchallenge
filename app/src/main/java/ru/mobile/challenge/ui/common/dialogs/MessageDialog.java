package ru.mobile.challenge.ui.common.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.azoft.injectorlib.InjectSavedState;
import ru.mobile.challenge.ui.base.dialogs.BaseDialog;
import ru.mobile.challenge.ui.base.fragments.BaseFragment;

public class MessageDialog extends BaseDialog {

    public static final String TAG = MessageDialog.class.getSimpleName();

    @InjectSavedState
    private String mTitle;
    @InjectSavedState
    private String mMessage;

    @InjectSavedState
    private String mPositiveButton;
    @InjectSavedState
    private String mNegativeButton;
    @InjectSavedState
    private String mNeutralButton;

    @InjectSavedState
    private int mDialogCode;

    public interface OnResultListener {
        void onResult(int dialogCode, Result result);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        setCancelable(true);

        if (mTitle != null) {
            builder.setTitle(mTitle);
        }
        if (mMessage != null) {
            builder.setMessage(mMessage);
        }
        if (mPositiveButton != null) {
            builder.setPositiveButton(mPositiveButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    onPositiveClick();
                }
            });
        }
        if (mNegativeButton != null) {
            builder.setNegativeButton(mNegativeButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    onNegativeClick();
                }
            });
        }
        if (mNeutralButton != null) {
            builder.setNeutralButton(mNeutralButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    onNeutralClick();
                }
            });
        }

        return builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        onResult(Result.CANCEL);
    }

    protected void onPositiveClick() {
        onResult(Result.POSITIVE);
    }

    protected void onNegativeClick() {
        onResult(Result.NEGATIVE);
    }

    protected void onNeutralClick() {
        onResult(Result.NEUTRAL);
    }

    private void onResult(Result result) {
        OnResultListener listener = null;
        if (getActivity() instanceof OnResultListener) {
            listener = (OnResultListener) getActivity();
        } else if (getTargetFragment() instanceof OnResultListener) {
            listener = (OnResultListener) getTargetFragment();
        }
        if (listener != null) {
            dismiss();
            listener.onResult(mDialogCode, result);
        }
    }

    public static Builder build(AppCompatActivity activity) {
        return new Builder(activity);
    }

    public static class Builder {

        private final AppCompatActivity mActivity;
        private final MessageDialog mDialog;

        Builder(AppCompatActivity activity) {
            mActivity = activity;
            mDialog = new MessageDialog();
        }

        Builder(AppCompatActivity activity, MessageDialog dialog) {
            mActivity = activity;
            mDialog = dialog;
        }

        public void show() {
            mDialog.show(mActivity.getSupportFragmentManager(), TAG);
        }

        public Builder title(int titleId) {
            mDialog.mTitle = mActivity.getString(titleId);
            return this;
        }

        public Builder title(String title) {
            mDialog.mTitle = title;
            return this;
        }

        public Builder message(int messageId) {
            message(mActivity.getString(messageId));
            return this;
        }

        public Builder message(String message) {
            mDialog.mMessage = message;
            return this;
        }

        public Builder positive(int buttonId) {
            positive(mActivity.getString(buttonId));
            return this;
        }

        public Builder positive(String button) {
            mDialog.mPositiveButton = button;
            return this;
        }

        public Builder negative(int buttonId) {
            negative(mActivity.getString(buttonId));
            return this;
        }

        public Builder negative(String button) {
            mDialog.mNegativeButton = button;
            return this;
        }

        public Builder neutral(int buttonId) {
            neutral(mActivity.getString(buttonId));
            return this;
        }

        public Builder neutral(String button) {
            mDialog.mNeutralButton = button;
            return this;
        }

        public Builder dialogCode(int dialogCode) {
            mDialog.mDialogCode = dialogCode;
            return this;
        }

        public Builder setTargetFragment(BaseFragment fragment, int requestCode) {
            mDialog.setTargetFragment(fragment, requestCode);
            return this;
        }

    }

    public enum Result {
        POSITIVE, NEGATIVE, NEUTRAL, CANCEL
    }

}
