package ru.mobile.challenge.ui.base.fragments;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import ru.mobile.challenge.BR;
import ru.mobile.challenge.ui.base.viewmodel.IBaseViewModel;

public abstract class BaseMvvmFragment<ViewModel extends IBaseViewModel, DataBinding extends ViewDataBinding>
        extends BaseFragment<DataBinding> {

    protected ViewModel viewModel;

    @Override
    @CallSuper
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = createViewModel(savedInstanceState);
    }

    @Override
    @CallSuper
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // We need always set this parameters, because they are not saved in BaseViewModel
        //noinspection unchecked
        viewModel.setUIView(this);
        viewModel.onViewModelSet();
    }

    @NonNull
    protected abstract ViewModel createViewModel(@Nullable final Bundle savedInstanceState);

    @Override
    @CallSuper
    protected void setupBinding(@NonNull final View view) {
        super.setupBinding(view);
        binding.setVariable(BR.viewModel, viewModel);
    }

    @Override
    @CallSuper
    public void onResume() {
        super.onResume();
        viewModel.onResume();
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        viewModel.onSaveInstanceState(outState);
    }

    @Override
    @CallSuper
    public void onPause() {
        viewModel.onPause();
        super.onPause();
    }

    @Override
    @CallSuper
    public void onDestroy() {
        viewModel.onDestroy();
        viewModel = null;
        super.onDestroy();
    }

}
