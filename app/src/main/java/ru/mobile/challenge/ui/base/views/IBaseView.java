package ru.mobile.challenge.ui.base.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

public interface IBaseView {

    void showLoading();

    void hideLoading();

    void showNetworkDialog();

    void showError(@StringRes final int errorId);

    void showError(@NonNull final String message);

    @Nullable
    Bundle getExtras();

}
