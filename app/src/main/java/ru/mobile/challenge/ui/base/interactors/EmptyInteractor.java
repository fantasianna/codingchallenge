package ru.mobile.challenge.ui.base.interactors;


import android.support.annotation.NonNull;

import ru.mobile.challenge.ui.base.viewmodel.IBaseViewModel;


public final class EmptyInteractor extends BaseInteractor<IBaseViewModel> {

    public EmptyInteractor(@NonNull final IBaseViewModel viewModel) {
        super(viewModel);
    }
}
