package ru.mobile.challenge.ui.common.dialogs;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import ru.mobile.challenge.R;
import ru.mobile.challenge.utils.GeneralUtils;


public class NetworkDialog extends MessageDialog {

    public static void show(@NonNull final AppCompatActivity activity) {
        //Builder builder = build(activity);
        Builder builder = new Builder(activity, new NetworkDialog());
        builder.message(R.string.error_need_internet);
        builder.positive(R.string.action_settings);
        builder.negative(R.string.action_ok);
        builder.show();
    }

    @Override
    protected void onPositiveClick() {
        GeneralUtils.openSettings();
    }

}
