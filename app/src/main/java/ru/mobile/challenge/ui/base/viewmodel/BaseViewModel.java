package ru.mobile.challenge.ui.base.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.azoft.injectorlib.InjectSavedState;
import com.azoft.injectorlib.Injector;
import ru.mobile.challenge.network.exceptions.NoNetworkException;
import ru.mobile.challenge.network.exceptions.RequestTimeoutException;
import ru.mobile.challenge.network.exceptions.UnknownException;
import ru.mobile.challenge.ui.base.interactors.IBaseInteractor;
import ru.mobile.challenge.ui.base.views.IBaseView;

/**
 * Base class for ViewModel in MVVM pattern.
 *
 * @param <View> View class to this ViewModel.
 */
public abstract class BaseViewModel<View extends IBaseView, Interactor extends IBaseInteractor>
        extends BaseObservable
        implements IBaseViewModel{

    /**
     * Enum for keep and update content state
     */
    public enum ContentState {UNDEFINED, PROGRESS, EMPTY, EXIST }

    private final Injector mInjector = Injector.init(getClass());
    /**
     * Interface for Activity/Fragment connecting.
     */
    private View mUIView;

    /**
     * Interface for Interactor connecting.
     */
    private Interactor mInteractor;

    /**
     * Keep and dynamically update content state
     */
    @InjectSavedState
    public ObservableField<ContentState> mContentState = new ObservableField<>(ContentState.UNDEFINED);


    public BaseViewModel(@NonNull final View uiView, @Nullable final Bundle savedInstanceState) {
        mUIView = uiView;
        mInjector.applyRestoreInstanceState(this, savedInstanceState);
        setupDataFromExtras();
        // Because some interactor needs data from extras.
        mInteractor = createInteractor();
    }

    @NonNull
    protected abstract Interactor createInteractor();

    private void setupDataFromExtras() {
        final Bundle extras = mUIView.getExtras();
        if (extras != null) {
            setupDataFromExtras(extras);
        }
    }

    /**
     * Setup data from activity's intent.
     *
     * @param extras instance.
     */
    @CallSuper
    protected void setupDataFromExtras(@NonNull final Bundle extras) {
        // If it's required.
    }

    @NonNull
    public final View getUIView() {
        return mUIView;
    }

    @Override
    public final void setUIView(@NonNull final IBaseView uiView) {
        //noinspection unchecked because of correct casting
        mUIView = (View) uiView;
    }

    @NonNull
    public Interactor getInteractor() {
        return mInteractor;
    }

    @Override
    public void onLoadingStarted() {
        mContentState.set(ContentState.PROGRESS);
        mUIView.showLoading();
    }

    @Override
    public void onLoadingError(@NonNull final Exception exception) {
        mUIView.hideLoading();
        mContentState.set(ContentState.EMPTY);
        if (exception instanceof RequestTimeoutException) {
            mUIView.showError(((RequestTimeoutException) exception).getErrorMessageId());
        } else if (exception instanceof NoNetworkException) {
            mUIView.showNetworkDialog();
        }
        else if (exception instanceof UnknownException){
            mUIView.showError(((UnknownException) exception).getErrorMessageId());
        }
    }


    @Override
    public void onLoadingError(@NonNull final String error) {
        mContentState.set(ContentState.EMPTY);
        mUIView.hideLoading();
        mUIView.showError(error);
    }

    @Override
    public void onLoadingError(final int code,  @StringRes final int errorId) {
        mContentState.set(ContentState.EMPTY);
        mUIView.hideLoading();
        mUIView.showError(errorId);
    }

    @Override
    public void onLoadingCompleted() {
        mUIView.hideLoading();
    }

    @Override
    public void onViewModelSet() {
        // Override if need
    }

    @Override
    @CallSuper
    public void onResume() {
        // Updating ViewModel data
        notifyChange();
    }

    @Override
    public void onSaveInstanceState(@NonNull final Bundle outState) {
        mInjector.applyOnSaveInstanceState(this, outState);
    }

    @Override
    @CallSuper
    public void onPause() {
        // Clear all subscriptions
        mInteractor.clearSubscriptions();
    }

    @Override
    @CallSuper
    public void onDestroy() {
        // Clear interface reference
        //noinspection ConstantConditions
        mUIView = null;
        mInteractor.onDestroy();
        //noinspection AssignmentToNull
        mInteractor = null;
    }
}
