package ru.mobile.challenge.ui.list;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import ru.mobile.challenge.R;
import ru.mobile.challenge.databinding.ItemImageBinding;
import ru.mobile.challenge.model.MovieModel;
import ru.mobile.challenge.ui.base.adapters.BaseAdapter;
import ru.mobile.challenge.ui.base.adapters.BaseViewHolder;

class ImagesAdapter extends BaseAdapter<MovieModel, ImagesAdapter.ViewHolder> {

    @Override
    protected ImagesAdapter.ViewHolder getItemViewHolder(@NonNull final ViewGroup viewGroup, final int type) {
        return new ImagesAdapter.ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_image, viewGroup, false));
    }

    class ViewHolder extends BaseViewHolder<MovieModel, ItemImageBinding> {

        ViewHolder(@NonNull final View view) {
            super(view);
        }

        @Override
        public void bind(@NonNull final MovieModel movieModel) {
            final String previewUrl = movieModel.getPosterUrl();
            if (!TextUtils.isEmpty(previewUrl)) {
                Glide.with(binding.imageView.getContext())
                        .load(previewUrl)
                        .apply(new RequestOptions().
                                centerCrop().
                                placeholder(R.drawable.image_placeholder))
                        .into(binding.imageView);
            }
        }
    }
}
