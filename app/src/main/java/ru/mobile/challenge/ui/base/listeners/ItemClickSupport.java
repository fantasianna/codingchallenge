package ru.mobile.challenge.ui.base.listeners;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.mobile.challenge.R;


public final class ItemClickSupport {

    private final RecyclerView mRecyclerView;
    private OnItemClickListener mOnItemClickListener;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(final View view) {
            if (mOnItemClickListener != null) {
                final RecyclerView.ViewHolder holder = mRecyclerView.getChildViewHolder(view);
                mOnItemClickListener.onItemClicked(mRecyclerView, holder.getAdapterPosition(), view);
            }
        }
    };
    private OnItemLongClickListener mOnItemLongClickListener;
    private final View.OnLongClickListener mOnLongClickListener = new View.OnLongClickListener() {

        @Override
        public boolean onLongClick(final View view) {
            if (mOnItemLongClickListener != null) {
                final RecyclerView.ViewHolder holder = mRecyclerView.getChildViewHolder(view);
                return mOnItemLongClickListener.onItemLongClicked(mRecyclerView, holder.getAdapterPosition(), view);
            }
            return false;
        }
    };
    private final RecyclerView.OnChildAttachStateChangeListener mAttachListener =
            new RecyclerView.OnChildAttachStateChangeListener() {

                @Override
                public void onChildViewAttachedToWindow(final View view) {
                    if (mOnItemClickListener != null) {
                        view.setOnClickListener(mOnClickListener);
                    }
                    if (mOnItemLongClickListener != null) {
                        view.setOnLongClickListener(mOnLongClickListener);
                    }
                }

                @Override
                public void onChildViewDetachedFromWindow(final View view) {
                    // do nothing
                }
            };

    private ItemClickSupport(@NonNull final RecyclerView recyclerView) {
        mRecyclerView = recyclerView;
        mRecyclerView.setTag(R.id.item_click_support, this);
        mRecyclerView.addOnChildAttachStateChangeListener(mAttachListener);
    }

    public static ItemClickSupport addTo(@NonNull final RecyclerView recyclerView) {
        ItemClickSupport support = (ItemClickSupport) recyclerView.getTag(R.id.item_click_support);
        if (support == null) {
            support = new ItemClickSupport(recyclerView);
        }
        return support;
    }

    public static ItemClickSupport removeFrom(@NonNull final RecyclerView recyclerView) {
        final ItemClickSupport support = (ItemClickSupport) recyclerView.getTag(R.id.item_click_support);
        if (support != null) {
            support.detach(recyclerView);
        }
        return support;
    }

    public ItemClickSupport setOnItemClickListener(@NonNull final OnItemClickListener listener) {
        mOnItemClickListener = listener;
        return this;
    }

    public ItemClickSupport setOnItemLongClickListener(@NonNull final OnItemLongClickListener listener) {
        mOnItemLongClickListener = listener;
        return this;
    }

    private void detach(@NonNull final RecyclerView recyclerView) {
        recyclerView.removeOnChildAttachStateChangeListener(mAttachListener);
        recyclerView.setTag(R.id.item_click_support, null);
    }

    public interface OnItemClickListener {
        void onItemClicked(@NonNull final RecyclerView recyclerView, final int position, @NonNull final View view);
    }

    public interface OnItemLongClickListener {
        @SuppressWarnings("BooleanMethodNameMustStartWithQuestion")
        boolean onItemLongClicked(@NonNull final RecyclerView recyclerView, final int position,
                                  @NonNull final View view);
    }
}
