package ru.mobile.challenge.ui.list;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.util.List;

import ru.mobile.challenge.R;
import ru.mobile.challenge.app.App;
import ru.mobile.challenge.databinding.ScreenListBinding;
import ru.mobile.challenge.model.MovieModel;
import ru.mobile.challenge.ui.base.activites.BaseMvvmActivity;
import ru.mobile.challenge.ui.details.DetailsActivity;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

public final class ListActivity
            extends BaseMvvmActivity<ListViewModel, ScreenListBinding>
            implements IListView {

    private ImagesAdapter mAdapter;

    @Override
    @CallSuper
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        setToolbarTitle(R.string.list_screen_title);

        mAdapter = new ImagesAdapter();
        binding.list.setAdapter(mAdapter);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        binding.list.setLayoutManager(gridLayoutManager);
        binding.list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull final RecyclerView recyclerView, final int dx, final int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final int visibleItemCount = gridLayoutManager.getChildCount();
                final int totalItemCount = gridLayoutManager.getItemCount();
                final int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    viewModel.tryLoadMore();
                }
            }
        });
    }

    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            final String query = intent.getStringExtra(SearchManager.QUERY);
            viewModel.search(query);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.screen_list;
    }

    @NonNull
    @Override
    protected ListViewModel createViewModel(@Nullable final Bundle savedInstanceState) {
        return new ListViewModel(this, savedInstanceState);
    }

    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);

        final SearchManager searchManager = (SearchManager) this.getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            searchView.setOnCloseListener(() -> {
                viewModel.loadMovies();
                return false;
            });

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(final String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(final String query) {
                    viewModel.search(query);
                    return false;
                }
            });

            final EditText searchEditText = searchView.findViewById(R.id.search_src_text);
            if (searchEditText != null) {
                searchEditText.setHintTextColor(ContextCompat.getColor(App.getContext(), R.color.white_transparent));
                searchEditText.setTextColor(ContextCompat.getColor(App.getContext(), R.color.white));
            }

            // Get the search close button image view
            final View closeButton = searchView.findViewById(R.id.search_close_btn);
            if (closeButton != null) {
                closeButton.setOnClickListener(view -> {
                    viewModel.loadMovies();
                    searchEditText.setText(null);
                });
            }

            searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {

                @Override
                public boolean onMenuItemActionExpand(final MenuItem item) {
                    //Nothing to do here
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(final MenuItem item) {
                    viewModel.loadMovies();
                    searchEditText.setText(null);
                    return true;
                }
            });

        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onItemClick(@NonNull final MovieModel movie) {
        final Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.EXTRA_MOVIE, movie);
        startActivityWithAnimation(intent);
    }

    @Override
    public void showMovies(@NonNull final List<MovieModel> movies) {
        binding.refreshLayout.setRefreshing(false);
        mAdapter.setData(movies);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void addMovies(@NonNull final List<MovieModel> movies) {
        mAdapter.addData(movies);
        mAdapter.notifyDataSetChanged();
    }
}
