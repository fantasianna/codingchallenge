package ru.mobile.challenge.ui.details;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.azoft.injectorlib.InjectSavedState;

import ru.mobile.challenge.model.MovieModel;
import ru.mobile.challenge.ui.base.interactors.EmptyInteractor;
import ru.mobile.challenge.ui.base.interactors.IBaseInteractor;
import ru.mobile.challenge.ui.base.viewmodel.BaseViewModel;


public final class DetailsViewModel extends BaseViewModel<IDetailsView, IBaseInteractor>
                                    implements IDetailsViewModel {

    @InjectSavedState
    private MovieModel mImageModel;

    DetailsViewModel(@NonNull final DetailsActivity baseView, @Nullable final Bundle savedInstanceState, @NonNull final MovieModel imageModel) {
        super(baseView, savedInstanceState);
        mImageModel = imageModel;
    }

    @NonNull
    @Override
    protected IBaseInteractor createInteractor() {
        return new EmptyInteractor(this);
    }

    @Override
    @CallSuper
    public void onViewModelSet() {
        super.onViewModelSet();
        if (mImageModel != null) {
            getUIView().showMovie(mImageModel);
        }
    }
}
