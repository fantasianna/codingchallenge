package ru.mobile.challenge.ui.base.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @param <Item>       item type
 * @param <ViewHolder> view holder type
 */
public abstract class BaseAdapter<Item, ViewHolder extends BaseViewHolder<Item, ?>>
        extends RecyclerView.Adapter<ViewHolder> {

    @NonNull
    private List<Item> mItems;

    public BaseAdapter(@NonNull final List<Item> items) {
        this();
        setData(items);
    }

    public BaseAdapter() {
        mItems = new ArrayList<>();
    }

    public void setData(@NonNull final List<Item> items) {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }


    public void addData(@NonNull final List<Item> items) {
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    @NonNull
    protected List<Item> getItems() {
        return Collections.unmodifiableList(mItems);
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int type) {
        return getItemViewHolder(viewGroup, type);
    }

    protected abstract ViewHolder getItemViewHolder(@NonNull final ViewGroup viewGroup, final int type);

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final Item item = mItems.get(i);
        viewHolder.bind(item);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}

