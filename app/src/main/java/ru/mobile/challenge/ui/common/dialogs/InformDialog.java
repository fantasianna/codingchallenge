package ru.mobile.challenge.ui.common.dialogs;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;

import ru.mobile.challenge.R;

public class InformDialog extends MessageDialog {

    public static void show(@NonNull final AppCompatActivity activity, @NonNull final String message) {
        Builder builder = build(activity);
        builder.message(message);
        builder.positive(R.string.action_ok);
        builder.show();
    }

    public static void show(@NonNull final AppCompatActivity activity, @StringRes final int messageId,
                            final int dialogCode) {
        Builder builder = build(activity);
        builder.message(messageId);
        builder.positive(R.string.action_ok);
        builder.dialogCode(dialogCode);
        builder.show();
    }

}
