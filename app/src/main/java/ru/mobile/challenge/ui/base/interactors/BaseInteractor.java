package ru.mobile.challenge.ui.base.interactors;


import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import ru.mobile.challenge.ui.base.viewmodel.IBaseViewModel;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public abstract class BaseInteractor<ViewModel extends IBaseViewModel> implements IBaseInteractor {

    /**
     * Container for RxJava subscriptions. Prevent leaking and crashes that comes from activity lifecycle events.
     */
    @NonNull
    private final CompositeSubscription mSubscriptions;
    private ViewModel mViewModel;

    protected BaseInteractor(@NonNull final ViewModel viewModel) {
        mViewModel = viewModel;
        mSubscriptions = new CompositeSubscription();
    }

    @NonNull
    public ViewModel getViewModel() {
        return mViewModel;
    }

    /**
     * Adds observable to CompositeSubscription for leak preventing.
     *
     * @param subscription instance.
     */
    @CallSuper
    protected final void addSubscription(@NonNull final Subscription subscription) {
        mSubscriptions.add(subscription);
    }

    @Override
    public void clearSubscriptions() {
        mSubscriptions.clear();
    }

    @Override
    public void onDestroy() {
        //noinspection AssignmentToNull
        mViewModel = null;
    }
}
