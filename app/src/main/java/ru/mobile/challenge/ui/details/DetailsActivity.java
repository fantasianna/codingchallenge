package ru.mobile.challenge.ui.details;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import ru.mobile.challenge.R;
import ru.mobile.challenge.databinding.ScreenDetailsBinding;
import ru.mobile.challenge.model.MovieModel;
import ru.mobile.challenge.ui.base.activites.BaseMvvmActivity;

public final class DetailsActivity
            extends BaseMvvmActivity<DetailsViewModel, ScreenDetailsBinding>
            implements IDetailsView {

    public static final String EXTRA_MOVIE = "DetailsActivity.EXTRA_MOVIE";

    @Override
    protected void setupToolbar() {
        super.setupToolbar();
        initToolbar();
        showBackButton();
        setToolbarTitle(R.string.details_screen_title);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.screen_details;
    }

    @NonNull
    @Override
    protected DetailsViewModel createViewModel(@Nullable final Bundle savedInstanceState) {
        final MovieModel imageModel = getDataFromIntent();
        return new DetailsViewModel(this, savedInstanceState, imageModel);
    }

    @Override
    public void showMovie(@NonNull final MovieModel movieModel) {
        final String previewUrl = movieModel.getBackdropUrl();
        if (!TextUtils.isEmpty(previewUrl)) {
            Glide.with(binding.imageView.getContext())
                    .load(previewUrl)
                    .apply(new RequestOptions().
                            centerCrop().
                            placeholder(R.drawable.image_placeholder))
                    .into(binding.imageView);
        }

        binding.title.setText(movieModel.getTitle());
        binding.overview.setText(movieModel.getOverview());
    }

    private MovieModel getDataFromIntent() {
        return (MovieModel) getIntent().getParcelableExtra(EXTRA_MOVIE);
    }
}
