package ru.mobile.challenge.network.exceptions;

import java.io.IOException;

import ru.mobile.challenge.R;

public class UnknownException extends IOException {

    private static final long serialVersionUID = 5559613103964855713L;

    public UnknownException() {
    }

    public UnknownException(String message) {
        super(message);
    }

    public UnknownException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownException(Throwable cause) {
        super(cause);
    }

    public int getErrorMessageId() {
        return R.string.error_something_wrong;
    }
}
