package ru.mobile.challenge.network;

import android.support.annotation.NonNull;

/**
 * Interface for every server model instance that should be converted.
 *
 * @param <T> model class
 */
public interface Convertable<T>  {

    @NonNull
    T convert();
}
