package ru.mobile.challenge.network.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PagedResponse<R>  {

    private List<R> results;
    private int page;
    @SerializedName("total_pages")
    private int totalPages;


    public List<R> getResults() {
        return results;
    }

    public int getPage() {
        return page;
    }

    public int getTotalPages() {
        return totalPages;
    }
}
