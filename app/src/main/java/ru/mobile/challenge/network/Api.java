package ru.mobile.challenge.network;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import retrofit2.http.Query;
import ru.mobile.challenge.BuildConfig;
import ru.mobile.challenge.R;
import ru.mobile.challenge.app.App;
import ru.mobile.challenge.network.responses.MovieResponse;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import ru.mobile.challenge.network.responses.PagedResponse;
import rx.Observable;

public interface Api {

    /**
     * Get movies model from the server
     * @return paged list
     */
    @GET("movie/now_playing")
    Observable<Response<PagedResponse<MovieResponse>>> getMovies(@Query("page") int page,
                                                                 @Query("api_key") String apiKey);


    /**
     * Get movies model from the server by search query
     * @return paged list
     */
    @GET("search/movie")
    Observable<Response<PagedResponse<MovieResponse>>> searchMovies(@Query("page") int page,
                                                                    @Query("query") String query,
                                                                    @Query("api_key") String apiKey);

    class ServerFactory {

        @NonNull
        public static Api createInstance() {
            final Context context = App.getContext();

            final OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();

            okHttpClientBuilder.readTimeout(context.getResources().getInteger(R.integer.read_time_out), TimeUnit.SECONDS);
            okHttpClientBuilder.connectTimeout(context.getResources().getInteger(R.integer.connect_time_out), TimeUnit.SECONDS);

            if (BuildConfig.DEBUG) {
                okHttpClientBuilder.addInterceptor(
                        new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
            }

            final OkHttpClient okHttpClient = okHttpClientBuilder.build();

            final Retrofit retrofit = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(context.getString(R.string.host))
                    .client(okHttpClient)
                    .build();

            return retrofit.create(Api.class);
        }
    }

}
