package ru.mobile.challenge.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public final class DataConverter {

    private static final SimpleSafeDateFormat SERVER_DATE_FORMAT = new SimpleSafeDateFormat("yyyy-MM-dd", Locale.US);

    private DataConverter() {
        // No instances
    }

    public static <Item, ServerItem extends Convertable<Item>> Item convertObject(
            @Nullable final ServerItem serverItem) {
        return serverItem == null ? null : serverItem.convert();
    }

    @NonNull
    public static <Item, ServerItem extends Convertable<Item>> List<Item> convertList(
            @Nullable final List<ServerItem> serverItems) {
        final List<Item> items = new ArrayList<>();
        if (serverItems != null) {
            //noinspection Convert2streamapi can't be implemeted
            for (ServerItem serverItem : serverItems) {
                items.add(serverItem.convert());
            }
        }
        return items;
    }

    public static Date convertServerDate(@NonNull final String date) {
        try {
            return SERVER_DATE_FORMAT.get()
                    .parse(date);
        } catch (@NonNull final ParseException e) {
            return null;
        }
    }

    private static class SimpleSafeDateFormat extends ThreadLocal<SimpleDateFormat> {

        private final String mPattern;
        private final Locale mLocale;

        public SimpleSafeDateFormat(@NonNull final String pattern, @NonNull final Locale locale) {
            mPattern = pattern;
            mLocale = locale;
        }

        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat(mPattern, mLocale);
        }
    }

}
