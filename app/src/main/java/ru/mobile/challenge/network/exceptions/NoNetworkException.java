package ru.mobile.challenge.network.exceptions;

import ru.mobile.challenge.R;

import java.io.IOException;

public class NoNetworkException extends IOException {

    private static final long serialVersionUID = 5559613103964855713L;

    public NoNetworkException() {
    }

    public NoNetworkException(String message) {
        super(message);
    }

    public NoNetworkException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoNetworkException(Throwable cause) {
        super(cause);
    }

    public int getErrorMessageId() {
        return R.string.error_need_internet;
    }
}
