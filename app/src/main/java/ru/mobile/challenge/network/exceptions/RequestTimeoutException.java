package ru.mobile.challenge.network.exceptions;

import ru.mobile.challenge.R;

import java.net.SocketTimeoutException;

public class RequestTimeoutException extends SocketTimeoutException {

    private static final long serialVersionUID = -8791928447101104801L;

    public RequestTimeoutException(String msg) {
        super(msg);
    }

    public RequestTimeoutException() {
    }

    public int getErrorMessageId() {
        return R.string.error_timeout;
    }
}
