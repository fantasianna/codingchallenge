package ru.mobile.challenge.network.responses;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import ru.mobile.challenge.app.App;
import ru.mobile.challenge.model.MovieModel;
import ru.mobile.challenge.network.Convertable;
import ru.mobile.challenge.network.DataConverter;

public class MovieResponse implements Convertable<MovieModel> {

    private String title;
    private String overview;
    @SerializedName("release_date")
    private String releaseDate;
    @SerializedName("poster_path")
    private String posterPath;
    @SerializedName("backdrop_path")
    private String backdropPath;

    @NonNull
    @Override
    public MovieModel convert() {
        final MovieModel movieModel = new MovieModel();
        movieModel.setTitle(title);
        movieModel.setOverview(overview);
        movieModel.setReleaseDate(releaseDate);
        movieModel.setImages(App.getContext(), posterPath, backdropPath);
        return movieModel;
    }
}
