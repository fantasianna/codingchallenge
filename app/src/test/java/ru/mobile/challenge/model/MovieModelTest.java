package ru.mobile.challenge.model;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;

public class MovieModelTest {

    @Test
    public void test_getTitle_validTitleValidDate_returnsFullTitle() {
        final MovieModel movieModel = new MovieModel();
        movieModel.setTitle("Name");
        movieModel.setReleaseDate("1999-12-01");

        assertThat(movieModel.getTitle(), is("Name (1999)"));
    }

    @Test
    public void test_getTitle_validTitleEmptyDate_returnsOnlyTitle() {
        final MovieModel movieModel = new MovieModel();
        movieModel.setTitle("Name");

        assertThat(movieModel.getTitle(), is("Name"));
    }

    @Test
    public void test_getTitle_emptyTitleEmptyDate_returnsEmptyTitle() {
        final MovieModel movieModel = new MovieModel();
        assertThat(movieModel.getTitle(), isEmptyOrNullString());
    }
}
