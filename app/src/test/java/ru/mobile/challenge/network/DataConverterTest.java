package ru.mobile.challenge.network;

import android.support.annotation.NonNull;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.nullValue;

public class DataConverterTest {

    // convertServerDate

    @Test
    public void test_convertServerDate_validString_returnsDate() {
        assertThat(DataConverter.convertServerDate("1970-01-01"), isA(Date.class));
    }

    @Test
    public void test_convertServerDate_invalidString_returnsNull() {
        assertThat(DataConverter.convertServerDate("invalidString"), nullValue());
    }

    @Test
    public void test_convertServerDate_EmptyString_returnsNull() {
        assertThat(DataConverter.convertServerDate(""), nullValue());
    }

    // convertObject

    @Test
    public void test_convertObject_ValidObject_returnsValidObject() {
        assertThat(DataConverter.convertObject(new TestServerResponse()),
                isA(TestModel.class));
    }

    @Test
    public void test_convertObject_NullObject_returnsNull() {
        assertThat(DataConverter.convertObject(null), nullValue());
    }

    // convertList

    @Test
    public void test_convertList_ValidList_returnsValidList() {
        final List<TestServerResponse> testServerResponses = new ArrayList<>();
        testServerResponses.add(new TestServerResponse(1));
        testServerResponses.add(new TestServerResponse(2));
        testServerResponses.add(new TestServerResponse(3));

        final List<TestModel> testModels = DataConverter.convertList(testServerResponses);

        assertThat(testModels, hasSize(3));

        assertThat(testModels.get(0).testField, equalTo(testServerResponses.get(0).testServerField));
        assertThat(testModels.get(1).testField, equalTo(testServerResponses.get(1).testServerField));
        assertThat(testModels.get(2).testField, equalTo(testServerResponses.get(2).testServerField));
    }

    @Test
    public void test_convertList_EmptyList_returnsEmptyList() {
        final List<TestServerResponse> testServerResponses = new ArrayList<>();
        final List<TestModel> testModels = DataConverter.convertList(testServerResponses);

        assertThat(testModels, hasSize(0));
    }


    @Test
    public void test_convertList_NullList_returnsEmptyList() {
        final List<TestModel> testModels = DataConverter.convertList(null);
        assertThat(testModels, hasSize(0));
    }

    // model for test

    class TestServerResponse implements Convertable<TestModel> {
        private int testServerField;

        TestServerResponse() {
        }

        TestServerResponse(final int testServerField) {
            this.testServerField = testServerField;
        }

        @NonNull
        @Override
        public TestModel convert() {
            return new TestModel(testServerField);
        }
    }

    class TestModel {
        private int testField;

        TestModel(final int testField) {
            this.testField = testField;
        }
    }

}
