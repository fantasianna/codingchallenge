package ru.mobile.challenge.ui.list;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import androidx.lifecycle.Lifecycle;
import androidx.test.core.app.ActivityScenario;
import ru.mobile.challenge.R;
import ru.mobile.challenge.ui.details.DetailsActivity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
public class ListActivityTest {

    @Test
    public void test_shouldUseCorrectTitle() {
        // GIVEN
        final ActivityScenario<ListActivity> scenario = ActivityScenario.launch(ListActivity.class);

        // WHEN
        scenario.moveToState(Lifecycle.State.CREATED);

        // THEN
        scenario.onActivity(activity -> {
            final Toolbar toolbar = activity.findViewById(R.id.toolbar);
            assertThat(toolbar, notNullValue());
            assertThat(toolbar.getTitle().toString(), equalTo("Movies"));
        });
    }

    @Test
    public void test_shouldInflateSearchMenu() {
        // GIVEN
        final ActivityScenario<ListActivity> scenario = ActivityScenario.launch(ListActivity.class);

        // WHEN
        scenario.moveToState(Lifecycle.State.CREATED);

        // THEN
        scenario.onActivity(activity -> {
            final Menu menu = shadowOf(activity).getOptionsMenu();

            //shadowOf(activity).clickMenuItem(R.id.action_search);
            assertThat(menu, notNullValue());
            assertThat(menu.findItem(R.id.action_search), notNullValue());
            assertThat(menu.findItem(R.id.action_search).getTitle(), equalTo("Search"));
        });
    }

    @Test
    public void test_clickingItem_shouldStartDetailsActivity() {
        // GIVEN
        final ActivityScenario<ListActivity> scenario = ActivityScenario.launch(ListActivity.class);

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED);

        // THEN
        scenario.onActivity(activity -> {
            final RecyclerView currentRecyclerView = activity.findViewById(R.id.list);
            currentRecyclerView.getChildAt(0).performClick();

            final Intent expectedIntent = new Intent(activity, DetailsActivity.class);
            final Intent actual = shadowOf(activity).getNextStartedActivity();
            assertThat(expectedIntent.getComponent(), equalTo(actual.getComponent()));
        });
    }

}
