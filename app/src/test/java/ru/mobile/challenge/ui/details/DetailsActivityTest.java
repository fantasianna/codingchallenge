package ru.mobile.challenge.ui.details;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import androidx.lifecycle.Lifecycle;
import androidx.test.core.app.ActivityScenario;
import ru.mobile.challenge.R;
import ru.mobile.challenge.app.App;
import ru.mobile.challenge.model.MovieModel;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
public class DetailsActivityTest {

    @Test
    public void test_shouldUseCorrectTitle() {
        // GIVEN
        final Intent intent = new Intent(App.getContext(), DetailsActivity.class);
        MovieModel movieModel = new MovieModel();
        movieModel.setTitle("Name");
        movieModel.setReleaseDate("1999-12-01");
        intent.putExtra(DetailsActivity.EXTRA_MOVIE, movieModel);
        final ActivityScenario<DetailsActivity> scenario = ActivityScenario.launch(intent);

        // WHEN
        scenario.moveToState(Lifecycle.State.CREATED);

        // THEN
        scenario.onActivity(activity -> {
            final Toolbar toolbar = activity.findViewById(R.id.toolbar);
            assertThat(toolbar, notNullValue());
            assertThat(toolbar.getTitle().toString(), equalTo("Movie Details"));
        });
    }

}
